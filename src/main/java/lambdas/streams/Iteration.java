package lambdas.streams;

import java.util.Arrays;
import java.util.List;

public class Iteration {
  public static void main(String[] args) {
    List<Integer> numbers = Arrays.asList(1,2,3,4,5,6,7,8,9,10);

    //external iterator
    for (int i = 0; i < numbers.size(); i++) {
      System.out.println(numbers.get(i));
    }

    //external iterator
    for (int e : numbers) {
      System.out.println(e);
    }

    //internal iterator
    numbers.forEach(number -> System.out.println(number));
    //parenthesis are optional for single parameter lambdas only

    numbers.forEach(System.out::println);

    //use lambdas as glue code, don't make them too lon
    // using {    return; } in lambdas is an anti-pattern
  }
}
