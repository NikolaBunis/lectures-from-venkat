package lambdas.streams;

public class JustTheEssence {
  public static void main(String[] args) {
    //functions have four things
    //1. name - not vital, function can be anonymous
    //2. parameter list
    //3. body
    //4. return type - can be inferred!
    Thread th = new Thread(() -> System.out.println("In another thread"));
    th.start();

    System.out.println("In main");
  }
}
