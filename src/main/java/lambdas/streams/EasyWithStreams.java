package lambdas.streams;

import java.util.stream.Stream;

public class EasyWithStreams {

  public static int compute(int k, int n) {
//    int result = 0;
//
//    int index = k;
//    int count = 0;
//    while (count < n) {
//      if (index % 2 == 0 && Math.sqrt(index) > 20) {
//        result += index * 2;
//        count++;
//      }
//      index++;
//    }
//    return result;
    return Stream.iterate(k, e -> e + 1)
        .filter(e -> e % 2 ==0)
        .filter(e -> Math.sqrt(e) > 20)
        .mapToInt(e -> e * 2)
        .limit(n)
        .sum();
  }

  public static void main(String[] args) {
    //Given a number K and a count N, find the total of the double of the N even numbers
    //starting with K, where sqrt of each number is > 20

    int k = 121;
    int n = 51;

    System.out.println(compute(k, n));
  }

}
