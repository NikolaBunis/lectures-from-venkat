package lambdas.streams;

import java.util.Arrays;
import java.util.List;

public class Parallel {
  public static void main(String[] args) {
    List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

    System.out.println(
        //numbers.stream()
        numbers.parallelStream()
            .filter(e -> e % 2 == 0)
            .mapToInt(Parallel::compute)
            .sum()
    );
  }

  private static int compute(Integer number) {
    //assume this is time intensive
    return number * 2;
  }
}
