package async.completable.future;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class GetVsGetNow1 {

  public static void main(String[] args) throws ExecutionException, InterruptedException {
    CompletableFuture<Integer> future = create();

    System.out.println("got it");

    System.out.println(future.get()); // bad idea
    //the best thing to do with get is to forget

    System.out.println("here");
  }

  public static CompletableFuture<Integer> create() {
    return CompletableFuture.supplyAsync(() -> compute());
  }

  private static int compute() {
    sleep(1000);
    return 2;
  }

  private static boolean sleep(int i) {
    try {
      Thread.sleep(i);
      return true;
    } catch (InterruptedException e) {
      e.printStackTrace();
      return false;
    }
  }
}

