package async.completable.future;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;

public class ThenApply {

  public static void main(String[] args) throws ExecutionException, InterruptedException {
// Stream                      CompletableFuture
// pipeline                    pipeline
// lazy                        lazy
// zero, one or more data      zero or one
// only data channel           data and error channel
// forEach                     thenAccept
// map                         thenApply
    create()
        .thenApply(data -> data * 10)
        .thenAccept(data -> System.out.println(data))
        .thenRun(() -> System.out.println("That went well"));
  }

  private static void printIt(int value) {
    System.out.println(value + " -- " + Thread.currentThread());
  }

  public static CompletableFuture<Integer> create() {
    ForkJoinPool pool = new ForkJoinPool(10);
    return CompletableFuture.supplyAsync(() -> compute(), pool);
  }

  private static int compute() {
//    sleep(1000);
    return 2;
  }

  private static boolean sleep(int i) {
    try {
      Thread.sleep(i);
      return true;
    } catch (InterruptedException e) {
      e.printStackTrace();
      return false;
    }
  }
}

