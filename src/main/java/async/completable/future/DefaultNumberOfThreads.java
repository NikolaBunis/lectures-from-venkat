package async.completable.future;

import java.util.Arrays;
import java.util.List;

public class DefaultNumberOfThreads {
 public static void main(String[] args) {
    List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,12,13,14,15,16,17,18,19,20);

    numbers.parallelStream()
    .map(e -> transform(e))
    .forEach(e -> {});
  }

  private static int add(int a, int b) {
  int result = a + b;
    System.out.println("a = " + a + " b = " + b + " result = " + result + " -- "
    + Thread.currentThread());
  return result;
  }

  private static void printIt(int number) {
    System.out.println("p: " + number + "--" + Thread.currentThread());
  }

  private static boolean check(int number) {
    System.out.println("c: " + number + "--" + Thread.currentThread());
    sleep(1000);
    return true;
  }

  private static int transform(int number) {
    System.out.println("t: " + number + "--" + Thread.currentThread());
    sleep(1000);
    return number * 1;
  }

  private static boolean sleep(int i) {
    try {
      Thread.sleep(i);
      return true;
    } catch (InterruptedException e) {
      e.printStackTrace();
      return false;
    }
  }

}
