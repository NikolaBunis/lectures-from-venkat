package async.completable.future;

import java.util.concurrent.CompletableFuture;

public class Sample {
  public static void main(String[] args) {
    create(2)
//        .thenApply(data -> inc(data))
        .thenCompose(data -> inc(data))
        .thenAccept(result -> System.out.println(result));
  }

  public static CompletableFuture<Integer> create(int number) {
    return CompletableFuture.supplyAsync(() -> number);
  }

  public static CompletableFuture<Integer> inc(int number) {
    return CompletableFuture.supplyAsync(() -> number + 1);
  }

  //function returns data - map
  //function returns stream - flatMap

  //async
  //function returns data - thenAccept/thenApply
  //function returns stream - thenCompose
}

