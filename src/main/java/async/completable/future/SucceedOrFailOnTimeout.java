package async.completable.future;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

public class SucceedOrFailOnTimeout {
  public static void main(String[] args) {
    CompletableFuture<Integer> future = new CompletableFuture<Integer>();

    //creating the pipeline without completing it on the spot
    future
        .thenApply(data -> data * 2)
        .exceptionally(throwable -> handleException(throwable))
        .thenApply(data -> data + 1)
        .thenAccept(data -> System.out.println(data));

    System.out.println("Built the pipeline!");

    future.orTimeout(2, TimeUnit.SECONDS);
//    future.completeOnTimeout(0, 2, TimeUnit.SECONDS);
    sleep(1000);

//    future.complete(2);

    sleep(2000);

    System.out.println("DONE");

    //Both in life and programming never do something without timeout
  }

  private static Integer handleException(Throwable throwable) {
    System.out.println("ERROR: " + throwable);
    return 0;
  }

  private static boolean sleep(int i) {
    try {
      Thread.sleep(i);
      return true;
    } catch (InterruptedException e) {
      e.printStackTrace();
      return false;
    }
  }
}

