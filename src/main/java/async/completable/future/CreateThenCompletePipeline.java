package async.completable.future;

import java.util.concurrent.CompletableFuture;

public class CreateThenCompletePipeline {

  public static void main(String[] args) {
    CompletableFuture<Integer> future = new CompletableFuture<Integer>();

    //creating the pipeline without completing it on the spot
    future.thenApply(data -> data * 2)
        .thenApply(data -> data + 1)
        .thenAccept(data -> System.out.println(data));

    System.out.println("Built the pipeline!");

    sleep(1000);

    //completing the pipeline in a different place from the creation
    future.complete(2);

    sleep(1000);
  }

  private static boolean sleep(int i) {
    try {
      Thread.sleep(i);
      return true;
    } catch (InterruptedException e) {
      e.printStackTrace();
      return false;
    }
  }
}

