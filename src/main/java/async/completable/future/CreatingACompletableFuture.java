package async.completable.future;

import java.util.concurrent.CompletableFuture;

public class CreatingACompletableFuture {

  public static void main(String[] args) {
create()
    .thenAccept(data -> System.out.println(data))
    .thenRun(() -> System.out.println("This never dies"))
    .thenRun(() -> System.out.println("This really never dies"));

    //Famous or popular functional interfaces
                                      //Stream
    //Supplier<T> T get()             - factories
    //Predicate<T> boolean test(T)    - filter
    //Function<T, R> R apply(T)       - map
    //Consumer<T> void accept(T)      - forEach
  }

  public static CompletableFuture<Integer> create() {
    return CompletableFuture.supplyAsync(() -> 2);
  }
}

