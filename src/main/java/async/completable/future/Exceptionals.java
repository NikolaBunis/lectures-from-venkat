package async.completable.future;

import java.util.concurrent.CompletableFuture;

public class Exceptionals {

  public static void main(String[] args) {

    //if there are not exceptions, data flow continues to the next "then" method
    //if there are exceptions, data flow continues to the next exceptional method
    //if the exception is handled, the data flow continues to the next "then" method
    //basically alternating between the data track and the error track
    create()
        .thenApply(data -> data * 2)
        .exceptionally(throwable -> handleException2(throwable))
        .thenAccept(data -> System.out.println(data))
        .exceptionally(throwable -> handleException(throwable));
  }

  private static Integer handleException2(Throwable throwable) {
    System.out.println("Error: " + throwable);
    return -1;
  }

  private static Void handleException(Throwable throwable) {
    System.out.println("Error: " + throwable);
    throw new RuntimeException("it is beyond all hope");
  }

  public static int compute() {
    throw new RuntimeException("something went wrong");
//    return 2;
  }

  public static CompletableFuture<Integer> create() {
    return CompletableFuture.supplyAsync(() -> compute());
  }
}

