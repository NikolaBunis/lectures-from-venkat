package async.completable.future;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class FlatMap {
  public static void main(String[] args) {
    List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

    numbers.stream()
//        .map(e -> func1(e)) //func1 is a one to one mapping function
//        .map(e -> func2(e)) //func2 is a one to many mapping function
        .flatMap(e -> Stream.of(func2(e)))
        .forEach(System.out::println);
  }
  //map one-to-one:  Stream<T> ===> Stream<Y>
  //map one-to-many: Stream<T> ===> Stream<List<Y>>
  //map one to many: Stream<T> ===> Stream<Y> - use flat map

  private static Object[] func2(int number) {
    return new Object[]{number -1, number + 1};
  }

  private static int func1(int number) {
    return number * 2;
  }
}

