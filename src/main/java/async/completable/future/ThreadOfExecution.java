package async.completable.future;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class ThreadOfExecution {

  public static void main(String[] args) throws ExecutionException, InterruptedException {
    System.out.println("In main " + Thread.currentThread());
    CompletableFuture<Integer> future = create();

    sleep(100);
    future.thenAccept(data -> printIt(data));

    System.out.println("depends on if the sleep in the compute() method is commented out or not");
  }

  private static void printIt(int value) {
    System.out.println(value + " -- " + Thread.currentThread());
  }

  public static CompletableFuture<Integer> create() {
    return CompletableFuture.supplyAsync(() -> compute());
  }

  private static int compute() {
    System.out.println("compute: " + Thread.currentThread());
//    sleep(1000);
    return 2;
  }

  private static boolean sleep(int i) {
    try {
      Thread.sleep(i);
      return true;
    } catch (InterruptedException e) {
      e.printStackTrace();
      return false;
    }
  }
}

