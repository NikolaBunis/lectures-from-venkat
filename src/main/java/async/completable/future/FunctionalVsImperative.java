package async.completable.future;

import java.util.Arrays;
import java.util.List;

public class FunctionalVsImperative {
  public static void main(String[] args) {
    //Martin Fowler: Collection Pipeline Pattern

    List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

    //find the total of double the even numbers

    int total = 0;

    for (int number : numbers) {
      if (number % 2 == 0) {
        total += number * 2;
      }
    }

    System.out.println(total);

    //imperative style has accidental complexity

    //functional style has less complexity overall and it is easier to parallelise

    //functional style - functional composition

    System.out.println(numbers.stream()
        .filter(e -> e % 2 == 0)
        .mapToInt(e -> e * 2)
        .sum());
  }
}
