package async.completable.future;

import java.util.Arrays;
import java.util.List;

public class OrderControl {

  //We solve one set of problems only to create a new set of problems

  //Java 1 - Threads
  //Java 5 - ExecutorServices
  //Pool induced deadlock
  //Workstealing - fork join pool
  //Java 7 - FJP
  //Common FJP - what parallelStream() uses

  public static void main(String[] args) {
    List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

    numbers.parallelStream()
        .map(e -> transform(e))
        .forEachOrdered(e -> printIt(e));
    //Some methods are inherently ordered
    //Some methods are ordered, but may have an ordered counterpart

  }
  private static void printIt(int number) {
    System.out.println("p: " + number + "--" + Thread.currentThread());
  }

  private static int transform(int number) {
    System.out.println("t: " + number + "--" + Thread.currentThread());
    sleep(1000);
    return number * 1;
  }

  private static boolean sleep(int i) {
    try{
      Thread.sleep(i);
      return true;
    } catch (InterruptedException e) {
      e.printStackTrace();
      return false;
    }
  }

}
