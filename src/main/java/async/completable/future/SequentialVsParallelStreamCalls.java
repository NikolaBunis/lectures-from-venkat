package async.completable.future;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class SequentialVsParallelStreamCalls {

  //in imperative style the structure of sequential code is very different from
  // the structure of concurrent code

  //using streams the structure of sequential code is identical to the structure of concurrent code

  public static void main(String[] args) {
    List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

    use(numbers.stream());
  }

  private static int transform(int number) {
    System.out.println("called for " + number);
    sleep(1000);
    return number * 1;
  }

  private static boolean sleep(int i) {
    return false;
  }

  public static void use(Stream<Integer> stream) {

    stream
        .parallel() //no op because of the sequential method below - the last one wins
        .map(e -> transform(e))
        .sequential()
        .forEach(System.out::println);
  }
}
